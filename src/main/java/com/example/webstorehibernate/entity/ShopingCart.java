package com.example.webstorehibernate.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ShopingCart {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Product> products;

    public ShopingCart() {
    }

    public ShopingCart(List<Product> products) {
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    @Override
    public String toString() {
        return "\nShopingCart{" +
                "id=" + id +
                ", products=" + products +
                '}';
    }
}
