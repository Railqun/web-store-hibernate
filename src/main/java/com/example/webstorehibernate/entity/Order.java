package com.example.webstorehibernate.entity;

import javax.persistence.*;
import java.util.Random;


@Entity
@Table(name = "qrder")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "credentials_id")
    private Credentials credentials;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "shoping_cart_id")
    private ShopingCart basket;

    private static int number = 0;
    private int status;
    private long start;
    private long diff;
    private long finish;

    public Order() {
        status = 0;
        start = System.currentTimeMillis();

        Random rand = new Random();
        diff = 1000 + rand.nextInt(5000);
        finish = start + diff;
    }

    public Order(Credentials credentials, ShopingCart basket) {
        this.credentials = credentials;
        this.basket = basket;

        status = 0;
        start = System.currentTimeMillis();

        Random rand = new Random();
        diff = 1000 + rand.nextInt(5000);
        finish = start + diff;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Credentials getPerson() {
        return credentials;
    }

    public void setPerson(Credentials credentials) {
        this.credentials = credentials;
    }

    public ShopingCart getBasket() {
        return basket;
    }

    public void setBasket(ShopingCart basket) {
        this.basket = basket;
    }

    public static int getNumber() {
        return number;
    }

    public static void setNumber(int number) {
        Order.number = number;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getDiff() {
        return diff;
    }

    public void setDiff(long diff) {
        this.diff = diff;
    }

    public long getFinish() {
        return finish;
    }

    public void setFinish(long finish) {
        this.finish = finish;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", credentials=" + credentials +
                ", basket=" + basket +
                ", status=" + status +
                '}';
    }

    public boolean check_time(long data_check) {
        //System.out.println("time_1 \t\t" + start);
        //System.out.println("diff \t\t" + diff);
        //System.out.println("time_finish \t" + finish);

        if((start + diff) < data_check) {
            status = 1;
            return true;
        }
        else
            return false;
    }

}
