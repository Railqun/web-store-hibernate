package com.example.webstorehibernate.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private int price;
    private String company;

    private int type;

    private static int count = 0;

    public Product() {
        create();
    }

    public Product(int price, String company, int type) {
        this.price = price;
        this.company = company;
        this.type = type;

        count++;
    }

    public void create() {
        name = UUID.randomUUID().toString();
        price = (int) (Math.random() * 10000);
        company = UUID.randomUUID().toString();
        count++;

        if(count % 2 == 0) type = 1;
        else type = 2;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getCompany() {
        return company;
    }

    public int getType() {
        return type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "\nProduct{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", company='" + company + '\'' +
                ", type=" + type +
                '}';
    }
}
