package com.example.webstorehibernate.store;

import org.apache.log4j.Logger;

import java.util.*;

public class Generate {
    List<Clothes> list_all = new ArrayList<>();
    TreeSet<UUID> FindId = new TreeSet<>();
    HashMap<Long, Order> TimeCreate = new HashMap<>();
    Orders<Order> orr = new Orders<>();

    private static final Logger LOGGER = Logger.getLogger(Generate.class.getSimpleName());

    public List<Clothes> getList_all() {
        return list_all;
    }

    public Orders<Order> getOrr() {
        return orr;
    }

    public Generate() {
        int i, j;

        LOGGER.info("Начало генерации");

        //генерация всех объектов
        System.out.println("Доступные товары");
        for(i = 0; i < 50; i++) {
            Clothes q;

            if(i % 2 == 0) q = new Shirt();
            else q = new Cap();

            list_all.add(q);
            FindId.add(q.getId());
            //q.read();
            System.out.println(q.getId());
        }

        //генерация пользователей с их товарами
        for(i = 0; i < 5; i++) {
            System.out.println("Создание пользователя " + i);
            Credentials per = new Credentials();
            ShopingCart<Clothes> basket = new ShopingCart<>();

            //System.out.println("Добавление объектов в корзину");
            for(j = 0; j < 10; j++)
                basket.add(list_all.get(i * 10 + j));

            //System.out.println("Удаление объекта");
            basket.delete(i);
            //показать все объекты в корзине
            basket.view_elements();

            //System.out.println("Создание заказа");
            Order item = new Order();
            item.create_order(per, basket);
            TimeCreate.put(item.getFinish(), item);
            orr.add(item);
        }

        //показать все заказы
        orr.view_elemensts();

        //обработка заказов
        System.out.println("Обработка заказов");
        orr.check();

        //показать все заказы после обработки
        orr.view_elemensts();

        long time = System.currentTimeMillis();
        System.out.println("time " + time);

        //удаление элементов используя коллекцию по времени создания

        Iterator <Map.Entry<Long, Order>> iter = TimeCreate.entrySet().iterator();
        while(iter.hasNext()) {
            Map.Entry<Long, Order> el = iter.next();
            if(time > el.getKey()) {
                orr.delete(el.getValue());
                iter.remove();
            }
        }

        //показать все заказы после обработки
        orr.view_elemensts();

        //поиск идентификатора в TreeSet
        UUID el[] = new UUID[2];

        el[0] = list_all.get(1).getId();
        el[1] = UUID.randomUUID();

        for(int k = 0; k < 2; k++) {
            if(FindId.contains(el[k])) {
                System.out.println("Товар есть id " + el[k]);
            }
            else {
                System.out.println("Товара нет id " + el[k]);
            }
        }
    }

    //создание и добавление товара в корзину с идентификатором id
    public void addClothes(int cardId) {
        int check = orr.isOrderById(cardId);

        if(check != -1) {
            Clothes addClothes;


            if(cardId % 2 == 0) addClothes = new Shirt();
            else addClothes = new Cap();

            LOGGER.info("Создан новый товар " + addClothes);

            list_all.add(addClothes);

            orr.getList().get(cardId).getBasket().add(addClothes);
            LOGGER.info("Товар добавлен " + addClothes);
        }
        else {
            LOGGER.warn("Нет корзины с заданным card_id " + cardId);
        }
    }

    public int delOrder(int orderId) {
        int check = orr.isOrderById(orderId);

        if(check != -1) {
            LOGGER.info("Удаление заказа с orderId " + orderId);
            orr.delete(check);
            return 0;
        }
        else {
            LOGGER.warn("Нет заказа для удаления с orderId " + orderId);
            return 1;
        }
    }
}
