package com.example.webstorehibernate.store;

public interface IcrudAction {
    void create();
    void read();
    void update();
    void delete();
}
