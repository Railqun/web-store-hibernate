package com.example.webstorehibernate.store;

import java.io.Serializable;
import java.util.Scanner;
import java.util.UUID;

public abstract class Clothes implements IcrudAction, Serializable {
    private static int count = 0;
    private UUID id;
    private String name;
    private int price;
    private String company;

    Clothes() {
        id = UUID.randomUUID();
        create();
    }

    Clothes(String sname, int sprice, String scompany) {
        id = UUID.randomUUID();
        name = sname;
        price = sprice;
        company = scompany;
        count++;
    }

    @Override
    public void create() {
        name = UUID.randomUUID().toString();
        price = (int) (Math.random() * 10000);
        company = UUID.randomUUID().toString();
        count++;
    }

    @Override
    public void delete() {
        id = null;
        name = "";
        price = 0;
        company = "";
        count--;
    }

    @Override
    public void read() {
        System.out.println("uuid = " + id);
        System.out.println("name = " + name);
        System.out.println("price = " + price);
        System.out.println("company = " + company);
    }

    @Override
    public void update() {
         Scanner in = new Scanner(System.in);

         System.out.println("new name");
         name = in.nextLine();

         System.out.println("new price");
         //price = in.nextInt();
         price = Integer.parseInt(in.nextLine());

         System.out.println("new company");
         company = in.nextLine();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public static int getCount() {
        return count;
    }

    UUID getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Clothes{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

