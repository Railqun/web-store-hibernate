package com.example.webstorehibernate.store;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

public class ShopingCart<T extends Clothes> implements Serializable {
    public ArrayList<T> getShop() {
        return shop;
    }

    private ArrayList<T> shop;

    public ShopingCart() {
        shop = new ArrayList<>();
    }

    public void add(T el) {
        shop.add(el);
    }

    public void delete(int index) {
        shop.remove(index);
    }

    public void view_elements() {
        Iterator iter = shop.iterator();
        //System.out.println("Корзина");
        while(iter.hasNext()) {
            T el = (T) iter.next();
            System.out.println(el.getId());
        }
    }

    public Clothes find(UUID id) {
        Iterator iter = shop.iterator();

        while(iter.hasNext()) {
            T el = (T) iter.next();
            if(el.getId().equals(id))
                return el;
        }

        return null;
    }
}
