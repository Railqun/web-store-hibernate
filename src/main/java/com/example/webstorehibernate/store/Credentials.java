package com.example.webstorehibernate.store;

import java.io.Serializable;
import java.util.UUID;

public class Credentials implements Serializable {
    private UUID id;
    private String name;
    private String surname;
    private String patronymic;
    private String email;

    public Credentials() {
        create();
    }

    private void create() {
        id = UUID.randomUUID();
        name = UUID.randomUUID().toString();
        surname = UUID.randomUUID().toString();
        patronymic = UUID.randomUUID().toString();
        email = UUID.randomUUID().toString();
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getEmail() {
        return email;
    }
}
