package com.example.webstorehibernate.controller;

import com.example.webstorehibernate.entity.Order;
import com.example.webstorehibernate.entity.Product;
import com.example.webstorehibernate.entity.ShopingCart;
import com.example.webstorehibernate.repos.OrderRepo;
import com.example.webstorehibernate.repos.ProductRepo;
import com.example.webstorehibernate.repos.ShopingCartRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class OrderController {

    private static final Logger LOGGER = Logger.getLogger(OrderController.class.getSimpleName());

    @Autowired
    ProductRepo productRepo;

    @Autowired
    OrderRepo orderRepo;

    @Autowired
    ShopingCartRepo shopingCartRepo;

    @GetMapping("command/readall")
    public Iterable<Order> getReadAll() {
        LOGGER.info("command = readall, вывод всех заказов");
        return orderRepo.findAll();
    }

    @GetMapping("command/readById")
    public Order getOrderById(@RequestParam(value = "order_id", required = true) Long orderId) {
        LOGGER.info("command = readById, вывод заказа с order_id = " + orderId);

        Optional<Order> order = orderRepo.findById(orderId);

        if(order.isPresent()) {
            return order.get();
        }
        else {
            LOGGER.warn("Нет заказа с orderId " + orderId);
            return null;
        }
    }

    @GetMapping("command/addToCard")
    public String addToCard(@RequestParam(value = "card_id", required = true) Long cardId) {
        String str = "command = addToCard, добавление товара в корзину с cardId = " + cardId;

        Optional<ShopingCart> shopingCart = shopingCartRepo.findById(cardId);

        if(shopingCart.isPresent()) {
            Product product = new Product();
            shopingCart.get().addProduct(product);

            shopingCartRepo.save(shopingCart.get());

            str += " прошло успешно";
            LOGGER.info(str);
        }
        else {
            str = "корзины с cardId = " + cardId + " нет";
            LOGGER.warn(str);
        }

        return str;
    }

    @GetMapping("command/delById")
    public String delById(@RequestParam(value= "order_id", required = true) String orderId) {
        String str = "command = delById, удаление заказа с order_id = " + orderId;

        LOGGER.info(str);

        Optional<Order> order = orderRepo.findById(Long.valueOf(orderId));

        if(order.isPresent()) {
            orderRepo.delete(order.get());
            str += "\n прошло успешно";
        }
        else{
            str = "Нет заказа для удаления с orderId " + orderId;
        }

        return str;
    }
}
