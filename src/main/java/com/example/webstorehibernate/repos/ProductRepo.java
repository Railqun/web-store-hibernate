package com.example.webstorehibernate.repos;

import com.example.webstorehibernate.entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepo extends CrudRepository<Product, Long> {
}
