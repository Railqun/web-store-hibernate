package com.example.webstorehibernate.repos;

import com.example.webstorehibernate.entity.Credentials;
import org.springframework.data.repository.CrudRepository;

public interface CredentialsRepo extends CrudRepository<Credentials, Long> {

}
