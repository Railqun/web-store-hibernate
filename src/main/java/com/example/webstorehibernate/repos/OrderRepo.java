package com.example.webstorehibernate.repos;

import com.example.webstorehibernate.entity.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepo extends CrudRepository<Order, Long>  {
}
