package com.example.webstorehibernate.repos;

import com.example.webstorehibernate.entity.ShopingCart;
import org.springframework.data.repository.CrudRepository;

public interface ShopingCartRepo extends CrudRepository<ShopingCart, Long> {
}
