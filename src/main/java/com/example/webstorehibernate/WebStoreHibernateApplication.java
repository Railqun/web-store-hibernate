package com.example.webstorehibernate;

import com.example.webstorehibernate.entity.Credentials;
import com.example.webstorehibernate.entity.Order;
import com.example.webstorehibernate.entity.Product;
import com.example.webstorehibernate.entity.ShopingCart;
import com.example.webstorehibernate.repos.CredentialsRepo;
import com.example.webstorehibernate.repos.OrderRepo;
import com.example.webstorehibernate.repos.ProductRepo;
import com.example.webstorehibernate.repos.ShopingCartRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class WebStoreHibernateApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(WebStoreHibernateApplication.class, args);
    }

    @Autowired
    private ProductRepo productRepo;

    @Autowired
    private ShopingCartRepo shopingCartRepo;

    @Autowired
    private CredentialsRepo credentialsRepo;

    @Autowired
    private OrderRepo orderRepo;

    private static final Logger LOGGER = Logger.getLogger(WebStoreHibernateApplication.class.getSimpleName());

    @Override
    public void run(String... args) throws Exception {

        Product product = new Product();
        Product product1 = new Product();

        ShopingCart shopingCart = new ShopingCart();
        shopingCart.setProducts(Arrays.asList(product, product1));

        Credentials credentials = new Credentials();

        Order order = new Order(credentials, shopingCart);

        orderRepo.save(order);

        generate();
    }

    void generate() {
        int i, j;

        LOGGER.info("Начало генерации");

        //генерация всех объектов
        System.out.println("Доступные товары");
        for(i = 0; i < 50; i++) {
            Product product = new Product();
            productRepo.save(product);
        }

            //list_all.add(q);
            //FindId.add(q.getId());
            //q.read();
            //System.out.println(q.getId());

        //генерация пользователей с их товарами
        /*for(i = 0; i < 5; i++) {
            System.out.println("Создание пользователя " + i);
            Credentials per = new Credentials();
            com.example.webstorehibernate.store.ShopingCart<Clothes> basket = new com.example.webstorehibernate.store.ShopingCart<>();

            //System.out.println("Добавление объектов в корзину");
            for(j = 0; j < 10; j++)
                basket.add(list_all.get(i * 10 + j));

            //System.out.println("Удаление объекта");
            basket.delete(i);
            //показать все объекты в корзине
            basket.view_elements();

            //System.out.println("Создание заказа");
            Order item = new Order();
            item.create_order(per, basket);
            TimeCreate.put(item.getFinish(), item);
            orr.add(item);
        }

        //показать все заказы
        orr.view_elemensts();

        //обработка заказов
        System.out.println("Обработка заказов");
        orr.check();

        //показать все заказы после обработки
        orr.view_elemensts();

        long time = System.currentTimeMillis();
        System.out.println("time " + time);*/
    }
}
